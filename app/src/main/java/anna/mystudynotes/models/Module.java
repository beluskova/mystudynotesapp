package anna.mystudynotes.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class Module {

	public String moduleId;
	public String uid;
	public String moduleName;
	public String note;
	public String gradingSchema;
	public double credits;
	public boolean current;

	public String _id;
	public String usertoken;
	public String address;
	public String googlephoto;
	public Location location = new Location();
	public double latitude;
	public double longitude;

	public Module() {}


	public Module(String uid, String moduleName, String note, String gradingSchema, double credits, boolean current, String token,String address, double lat, double lng, String path)
	{
		this.uid = uid;
		this.moduleName = moduleName;
		this.note = note;
		this.gradingSchema = gradingSchema;
		this.credits = credits;
		this.current = current;

		this.usertoken = token;
		this.address = address;
		this.location.latitude = lat;
		this.location.longitude = lng;
		this.latitude = lat;
		this.longitude = lng;
		this.googlephoto = path;
	}

	@Override
	public String toString() {
		return "Module [name=" + moduleName
				+ ", note =" + note + ", gradingSchema =" + gradingSchema + ", credits =" + credits
				+ ", current  =" + current + " "
				+ usertoken + " " + address + " " + location.latitude + " " + location.longitude + "]";
	}

	@Exclude
	public Map<String, Object> toMap() {
		HashMap<String, Object> result = new HashMap<>();

		result.put("uid", uid);
		result.put("moduleName", moduleName);
		result.put("note", note);
		result.put("gradingSchema", gradingSchema);
		result.put("credits", credits);
		result.put("current", current);

		result.put("usertoken", usertoken);
		result.put("address", address);
		result.put("latitude", latitude);
		result.put("longitude", longitude);
		result.put("googlephoto", googlephoto);

		return result;
	}
}
