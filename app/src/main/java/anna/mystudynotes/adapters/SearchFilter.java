package anna.mystudynotes.adapters;

import anna.mystudynotes.api.DBManager;
import anna.mystudynotes.fragments.ModuleFragment;
import anna.mystudynotes.main.MyStudyNotesApp;

import android.widget.Filter;

import com.google.firebase.database.Query;

public class SearchFilter extends Filter {
    private String 				filterText;
    private ModuleListAdapter 	           adapter;
    private boolean current = false;
    private Query query;
    private ModuleFragment fragment;


    public SearchFilter(Query originalModuleQuery, String filterText,
                        ModuleListAdapter adapter, ModuleFragment fragment) {
        super();
        this.query = originalModuleQuery;
        this.filterText = filterText;
        this.adapter = adapter;
        this.fragment = fragment;
    }

    public void setFilter(String filterText) {
        this.filterText = filterText;
    }


    @Override
    protected FilterResults performFiltering(CharSequence prefix) {
        FilterResults results = new FilterResults();

        if(prefix != null)
            if (prefix.length() > 0)
                query = fragment.app.dbManager.noteFilter(prefix.toString());

            else {
                if (filterText.equals("all")) {
                    query = fragment.app.dbManager.getAllModules();
                } else if (filterText.equals("current")) {
                    query = fragment.app.dbManager.getCurrentModules();
                }
            }
        results.values = query;

        return results;
    }

    @Override
    protected void publishResults(CharSequence prefix, FilterResults results) {

        fragment.updateUI((Query) results.values);
    }
}
