package anna.mystudynotes.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

import anna.mystudynotes.R;
import anna.mystudynotes.models.Module;


public class ModuleItem {
    View view;

    public ModuleItem(Context context, ViewGroup parent,
                      OnClickListener deleteListener, Module module) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.modulerow, parent, false);
        view.setTag(module._id);

        updateControls(module);

        ImageView imgDelete = (ImageView) view.findViewById(R.id.imgDelete);
        imgDelete.setTag(module);
        imgDelete.setOnClickListener(deleteListener);
    }

    private void updateControls(Module module) {
        ((TextView) view.findViewById(R.id.rowModuleName)).setText(module.moduleName);
        ((TextView) view.findViewById(R.id.rowModuleNote)).setText(module.note);
        ((TextView) view.findViewById(R.id.rowGradingSchema)).setText(module.gradingSchema);
        ((TextView) view.findViewById(R.id.rowCredits)).setText(
                new DecimalFormat("0") .format(module.credits));
        ImageView imgIcon = (ImageView) view.findViewById(R.id.RowImage);

        if (module.current == true)
            imgIcon.setImageResource(R.drawable.icon_current_yes);
        else
            imgIcon.setImageResource(R.drawable.icon_current_no);
    }
}