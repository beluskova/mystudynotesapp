package anna.mystudynotes.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.database.Query;

import java.text.DecimalFormat;

import anna.mystudynotes.R;
import anna.mystudynotes.models.Module;

public class ModuleListAdapter extends FirebaseListAdapter<Module> {
    private Context context;
    private OnClickListener deleteListener;
    public Query query;

    public ModuleListAdapter(Activity context, OnClickListener deleteListener,
                             Query query) {
        super(context, Module.class, R.layout.modulerow, query);
        Log.v("Module List Adapter: ", "Creating Adapter with query: " + query);
        this.context = context;
        this.deleteListener = deleteListener;
        this.query = query;
    }

    @Override
    protected void populateView(View row, Module module, int position) {
        Log.v("coffeemate", "Populating View Adapter with :" + module);
        //Set the rows TAG to the module 'key'
        row.setTag(getRef(position).getKey());

        ((TextView) row.findViewById(R.id.rowModuleName)).setText(module.moduleName);
        ((TextView) row.findViewById(R.id.rowModuleNote)).setText(module.note);
        ((TextView) row.findViewById(R.id.rowGradingSchema)).setText(module.gradingSchema);
        ((TextView) row.findViewById(R.id.rowCredits)).setText(
                new DecimalFormat ("0") .format(module.credits));

        ImageView imgIcon = (ImageView) row.findViewById(R.id.RowImage);

        if (module.current == true)
            imgIcon.setImageResource(R.drawable.icon_current_yes);
        else
            imgIcon.setImageResource(R.drawable.icon_current_no);


        ImageView imgDelete = (ImageView) row.findViewById(R.id.imgDelete);
        imgDelete.setTag(getRef(position).getKey());
        imgDelete.setOnClickListener(deleteListener);
    }
}


