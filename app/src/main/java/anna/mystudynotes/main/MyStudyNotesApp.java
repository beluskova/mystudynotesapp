package anna.mystudynotes.main;

import android.app.Application;
import android.graphics.Bitmap;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

import anna.mystudynotes.api.DBManager;
import anna.mystudynotes.models.Module;


public class MyStudyNotesApp extends Application
{
    private static MyStudyNotesApp mInstance;
    public List <Module>  moduleList = new ArrayList<Module>();

    /* Client used to interact with Google APIs. */
    public GoogleApiClient      mGoogleApiClient;
    public GoogleSignInOptions  mGoogleSignInOptions;
    public Location             mCurrentLocation;
    public FirebaseAuth mFirebaseAuth;
    public FirebaseUser mFirebaseUser;

    public DBManager dbManager;


    public boolean signedIn = false;
    public String googleToken;
    public String googleName;
    public String googleMail;
    public String googlePhotoURL;
    public Bitmap googlePhoto;
    public int drawerID = 0;

    public static final String TAG = MyStudyNotesApp.class.getName();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "MyStudyNotes App Started");
        mInstance = this;
        dbManager = new DBManager();
        dbManager.open();
        Log.v("Study Notes:", "Firebase Database Connected");
    }

    public static synchronized MyStudyNotesApp getInstance() {
        return mInstance;
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}