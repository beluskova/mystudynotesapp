package anna.mystudynotes.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;

import anna.mystudynotes.R;
import anna.mystudynotes.activities.Home;
import anna.mystudynotes.api.DBListener;
import anna.mystudynotes.main.MyStudyNotesApp;
import anna.mystudynotes.models.Module;


public class EditFragment extends Fragment implements DBListener {

    private OnFragmentInteractionListener mListener;
    TextView    titleBar, titleName, titleNote;
    Module aModule;
    Boolean     isCurrent;
    double moduleCredits;
    EditText    name, note, credits, schema;
    ImageView   currentImage;

    String      moduleKey;

    public MyStudyNotesApp app = MyStudyNotesApp.getInstance();

    public EditFragment() {
        // Required empty public constructor
    }

    public static EditFragment newInstance(Bundle moduleBundle) {
        EditFragment fragment = new EditFragment();
        fragment.setArguments(moduleBundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit, container, false);

        titleBar = ((TextView)v.findViewById(R.id.moduleNameTextView));
        titleNote = ((TextView)v.findViewById(R.id.noteTextView));
        name = (EditText)v.findViewById(R.id.nameEditText);
        note = (EditText)v.findViewById(R.id.noteEditText);
      //  schema = (EditText)v.findViewById(R.id.gradingSchemaTextView);
        credits = (EditText)v.findViewById(R.id.creditsEditText);
        currentImage = (ImageView) v.findViewById(R.id.currentImageView);

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        app.dbManager.attachListener(this);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        app.dbManager.attachListener(this);
        if(getArguments() != null) {
            moduleKey = getArguments().getString("moduleKey");
            Log.v("Edit Fragment","ON RESUME edit a module with key: " + moduleKey );
            app.dbManager.getAModule(moduleKey);
        }
      //  titleBar = (TextView) getActivity().findViewById(R.id.recentAddedBarTextView);
        //titleBar.setText(aModule.moduleName);
    }

    @Override
    public void onSuccess(DataSnapshot dataSnapshot) {
        aModule = dataSnapshot.getValue(Module.class);
        Log.v("Edit Fragment","edit a module: " + aModule + " for user : " + app.dbManager.mFBUserId);
        if(aModule != null)
            Log.v("Edit Fragment","ON SUCCESS edit a module with key: " + moduleKey );
            updateUI();

    }

    @Override
    public void onFailure() {
        Log.v("Edit Fragment", "Unable to Read/Update Module Data");
        Toast.makeText(getActivity(),"Unable to Read/Update Module Data",Toast.LENGTH_LONG).show();

    }


    public interface OnFragmentInteractionListener {
        void toggle(View v);
        void update(View v);
    }

    public void toggle(View v) {

        if (isCurrent) {
            aModule.current = false;
            toastMessage("Removed From Current Modules");
            isCurrent = false;
            currentImage.setImageResource(R.drawable.icon_current_no);
        } else {
            aModule.current = true;
            toastMessage("Added to Current Modules !!");
            isCurrent = true;
            currentImage.setImageResource(R.drawable.icon_current_yes);
        }

        app.dbManager.toggleFavourite(moduleKey,isCurrent);
    }

    public void update(View v) {

        String moduleName = name.getText().toString();
        String moduleNote = note.getText().toString();
//        String gradingSchema = schema.getText().toString();

        try {
            moduleCredits = Double.parseDouble(credits.getText().toString());
        } catch (NumberFormatException e)
        {            moduleCredits = 0;        }

            aModule.moduleName = moduleName;
            aModule.note = moduleNote;
         //   aModule.gradingSchema = gradingSchema;
            aModule.credits = moduleCredits;

        if ((aModule.moduleName.length() > 0) && (aModule.note.length() > 0)) {

            app.dbManager.updateAModule(moduleKey,aModule);
            //return to the home screen
            Intent intent = new Intent(getActivity(), Home.class);
            getActivity().startActivity(intent);

        } else
            toastMessage("You must Enter Something for Name and Shop");
    }

    protected void toastMessage(String s) {
        Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
    }

    public void updateUI() {

        titleBar.setText(aModule.moduleName);
       // titleNote.setText(aModule.note);
        name.setText(aModule.moduleName);
        note.setText(aModule.note);
//        schema.setText(aModule.gradingSchema);
        credits.setText(""+aModule.credits);


        if (aModule.current == true) {
            currentImage.setImageResource(R.drawable.icon_current_yes);
            isCurrent = true;
        } else {
            currentImage.setImageResource(R.drawable.icon_current_no);
            isCurrent = false;
        }
    }

}

