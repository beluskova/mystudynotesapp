package anna.mystudynotes.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import anna.mystudynotes.R;
import anna.mystudynotes.activities.Home;
import anna.mystudynotes.api.DBManager;

import anna.mystudynotes.main.MyStudyNotesApp;
import anna.mystudynotes.models.Module;

import static anna.mystudynotes.main.MyStudyNotesApp.TAG;

public class AddFragment extends Fragment implements View.OnClickListener ,OnMapReadyCallback {

    private TextView    titleBar;
    private String 		moduleName, moduleNote, gradingSchema;
    private double 		moduleCredits;
    private EditText    name, note, credits, schema;
    public MyStudyNotesApp app = MyStudyNotesApp.getInstance();
    public DBManager dbManager;

    public AddFragment() {
        // Required empty public constructor
    }

    public static AddFragment newInstance() {
        AddFragment fragment = new AddFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add, container, false);

        Button saveButton = (Button) v.findViewById(R.id.saveModuleBtn);
        name = (EditText) v.findViewById(R.id.moduleNameEditText);
        note = (EditText) v.findViewById(R.id.noteEditText);
        credits = (EditText) v.findViewById(R.id.creditsEditText);
        schema = (EditText) v.findViewById(R.id.gradingSchemaEditText);
        saveButton.setOnClickListener(this);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        titleBar = (TextView) getActivity().findViewById(R.id.recentAddedBarTextView);
        titleBar.setText(R.string.addAModuleLbl);
    }

    public void onClick(View v) {

        moduleName = name.getText().toString();
        moduleNote = note.getText().toString();
        gradingSchema = schema.getText().toString();
        try {
            moduleCredits = Double.parseDouble(credits.getText().toString());
        } catch (NumberFormatException e) {
            moduleCredits = 0;
        }

        if ((moduleName.length() > 0) && (moduleNote.length() > 0)) {
            Module m = new Module(app.dbManager.mFBUserId, moduleName, moduleNote,
                    gradingSchema,
                     moduleCredits,
                    false,
                    app.googleToken,
                    getAddressFromLocation( app.mCurrentLocation ),
                    app.mCurrentLocation.getLatitude(),
                    app.mCurrentLocation.getLongitude(),
                    app.googlePhotoURL);

            app.dbManager.addModule(m);
            resetFields();
            Toast.makeText(
                    getActivity(),
                    "A new coffee " + moduleName + " added, returning to home view",
                    Toast.LENGTH_SHORT).show();
            //returning to home page
            Intent intent = new Intent(getActivity(), Home.class);
            startActivity(intent);

        } else
            Toast.makeText(
                    getActivity(),
                    "You must Enter Something for "
                            + "\'Name\', \'Note\' and \'Credits\'",
                    Toast.LENGTH_SHORT).show();
    }

    private void resetFields() {
        name.setText("");
        note.setText("");
        credits.setText("");
        schema.setText("");
        name.requestFocus();
        name.setFocusable(true);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        addModules(app.moduleList,googleMap);
    }

    public void addModules(List<Module> list, GoogleMap mMap)
    {
        for(Module m : list)
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(m.location.latitude, m.location.longitude))
                    .title(moduleName + " " + m.credits + " credits")
                    .snippet(m.note + " " + m.address)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.notebook)));

    }

    private String getAddressFromLocation( Location location ) {
        Geocoder geocoder = new Geocoder( getActivity() );

        String strAddress = "";
        Address address;
        try {
            address = geocoder
                    .getFromLocation( location.getLatitude(), location.getLongitude(), 1 )
                    .get( 0 );
            strAddress = address.getAddressLine(0) +
                    " " + address.getAddressLine(1) +
                    " " + address.getAddressLine(2);
        }
        catch (IOException e ) {
        }

        return strAddress;
    }
}

