package anna.mystudynotes.fragments;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.Query;

import anna.mystudynotes.R;
import anna.mystudynotes.adapters.ModuleListAdapter;
import anna.mystudynotes.api.DBManager;


import anna.mystudynotes.adapters.ModuleFilter;

import anna.mystudynotes.main.MyStudyNotesApp;
import anna.mystudynotes.models.Module;


import static anna.mystudynotes.main.MyStudyNotesApp.TAG;

public class ModuleFragment extends Fragment implements AdapterView.OnItemClickListener,
        View.OnClickListener,
        AbsListView.MultiChoiceModeListener
{
  protected static ModuleListAdapter listAdapter;
  protected         ListView            listView;
  protected         ModuleFilter        moduleFilter;
  public            boolean             current = false;
  protected         TextView            titleBar;
  protected         SwipeRefreshLayout  mSwipeRefreshLayout;
  public            Query               query;
  public            View.OnClickListener  deleteListener;
  public            String                moduleKey;

  public MyStudyNotesApp app = MyStudyNotesApp.getInstance();

  public ModuleFragment() {
    // Required empty public constructor
  }

  public static ModuleFragment newInstance() {
    ModuleFragment fragment = new ModuleFragment();
    return fragment;
  }
  @Override
  public void onAttach(Context context)
  {
    super.onAttach(context);
    app = (MyStudyNotesApp) getActivity().getApplication();
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    app = (MyStudyNotesApp) getActivity().getApplication();
    View v = null;
    v = inflater.inflate(R.layout.fragment_home, container, false);
    listView = (ListView) v.findViewById(R.id.moduleList);

    mSwipeRefreshLayout =   (SwipeRefreshLayout) v.findViewById(R.id.module_swipe_refresh_layout);
    setSwipeRefreshLayout();

    deleteListener = new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (view.getTag() instanceof Module)
        {
          onModuleDelete ((Module) view.getTag());
        }
      }
    };
    return v;
  }

  protected void setSwipeRefreshLayout()
  {
    mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {
        query = app.dbManager.getAllModules();
        Log.v(TAG, "Module Fragment: setSwipeRefreshLayout " + mSwipeRefreshLayout) ;
      }
    });
  }

  @Override
  public void onResume() {
    super.onResume();
    app.mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
   query = app.dbManager.getAllModules();
    if(current)
      query = app.dbManager.getCurrentModules();

    Log.v(TAG, "moduleFrgament: onResume: getAll " + app.dbManager.getAllModules()) ;
    updateUI(query);

  }

  @Override
  public void onPause() {
    super.onPause();
  }

  public void updateUI(Query query) {

    titleBar = (TextView)getActivity().findViewById(R.id.recentAddedBarTextView);
    titleBar.setText(R.string.recentlyViewedLbl);

    listAdapter = new ModuleListAdapter(getActivity(), deleteListener, query);
    moduleFilter = new ModuleFilter(query,"all",listAdapter,this);
    setListView(listView);

    if (current) {
        query = app.dbManager.getCurrentModules();
      }

    if(app.moduleList.isEmpty())
      ((TextView)getActivity().findViewById(R.id.empty_list_view)).setText(R.string.recentlyViewedEmptyMessage);
    else
      ((TextView)getActivity().findViewById(R.id.empty_list_view)).setText("");

    listAdapter.notifyDataSetChanged(); // Update the adapter
  }

  public void setListView(ListView listview) {

    listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
    listview.setMultiChoiceModeListener(this);
    listview.setAdapter (listAdapter);
    listview.setOnItemClickListener(this);
    listview.setEmptyView(getActivity().findViewById(R.id.empty_list_view));
  }

  @Override
  public void onDetach() {
    super.onDetach();
  }

  @Override
  public void onStart()
  {
    super.onStart();
  //  validateFirebaseUser();
//    app.dbManager.mFBUserId = app.mFirebaseUser.getUid();
    Log.v(TAG,"Calling user from module fragment " );
  }

  @Override
  public void onClick(View view)
  {
    if (view.getTag() instanceof Module)
    {
      onModuleDelete ((Module) view.getTag());
    }
  }

  public void onModuleDelete(final Module module)
  {
    String stringName = module.moduleName;
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setMessage("Are you sure you want to Delete the \'Module\' " + stringName + "?");
    builder.setCancelable(false);

    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface dialog, int id) {
       moduleKey = getArguments().getString("moduleKey");
        app.dbManager.deleteAModule(moduleKey);
        query = app.dbManager.getAllModules();
      }
    }).setNegativeButton("No", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface dialog, int id)
      {
        dialog.cancel();
      }
    });
    AlertDialog alert = builder.create();
    alert.show();
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    Bundle activityInfo = new Bundle();
    activityInfo.putString("moduleKey", (String) view.getTag());

    FragmentTransaction ft = getFragmentManager().beginTransaction();
    Fragment fragment = EditFragment.newInstance(activityInfo);
    ft.replace(R.id.homeFrame, fragment);
    ft.addToBackStack(null);
    ft.commit();
  }

  /* ************ MultiChoiceModeListener methods (begin) *********** */
  @Override
  public boolean onCreateActionMode(ActionMode actionMode, Menu menu)
  {
    MenuInflater inflater = actionMode.getMenuInflater();
    inflater.inflate(R.menu.delete_list_context, menu);
    return true;
  }

  @Override
  public boolean onPrepareActionMode(ActionMode actionMode, Menu menu)
  {
    return false;
  }

  @Override
  public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem)
  {
    switch (menuItem.getItemId())
    {
      case R.id.menu_item_delete_module:
        deleteModules(actionMode);
        return true;
      default:
        return false;
    }
  }

  public void deleteModules(ActionMode actionMode)
  {
    for (int i = listAdapter.getCount() - 1; i >= 0; i--) {
      if (listView.isItemChecked(i)) {
        app.dbManager.deleteAModule(listView.getChildAt(i).getTag().toString());
      }
       query = app.dbManager.getAllModules();
    }
    actionMode.finish();

    if (current) {
      //Update the filters data
      query = app.dbManager.getCurrentModules();
      moduleFilter = new ModuleFilter(query,"current",listAdapter,this);
      moduleFilter.filter(null);
    }
    listAdapter.notifyDataSetChanged();
  }

  @Override
  public void onDestroyActionMode(ActionMode actionMode)
  {}

  @Override
  public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked)
  {}
  /* ************ MultiChoiceModeListener methods (end) *********** */
}

