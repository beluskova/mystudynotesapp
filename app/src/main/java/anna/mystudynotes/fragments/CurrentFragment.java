package anna.mystudynotes.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


import anna.mystudynotes.R;
import anna.mystudynotes.adapters.ModuleFilter;


public class CurrentFragment extends ModuleFragment
        implements AdapterView.OnItemSelectedListener, TextWatcher {

    String selected;

    public CurrentFragment() {
        // Required empty public constructor
    }

    public static CurrentFragment newInstance() {
        CurrentFragment fragment = new CurrentFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context c) {
        super.onAttach(c);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater,container,savedInstanceState);

        View v = inflater.inflate(R.layout.fragment_current, container, false);


        listView = (ListView) v.findViewById(R.id.moduleList); //Bind to the list on our Search layout

        setListView(listView);

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.module_swipe_refresh_layout);
        setSwipeRefreshLayout();

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        titleBar = (TextView)getActivity().findViewById(R.id.recentAddedBarTextView);
        titleBar.setText(R.string.searchModulesLbl);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        selected = parent.getItemAtPosition(position).toString();
        checkSelected(selected);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}

    @Override
    public void deleteModules(ActionMode actionMode) {
        super.deleteModules(actionMode);
        checkSelected(selected);
    }

    private void checkSelected(String selected)
    {
        if(moduleFilter == null)
            moduleFilter = new ModuleFilter(query,"all",listAdapter,this);


        if (selected != null) {
            if (selected.equals("All Modules")) {
                moduleFilter.setFilter("all");
            } else if (selected.equals("Current")) {
                moduleFilter.setFilter("current");
            }

            String filterText = ((EditText)getActivity()
                    .findViewById(R.id.searchModuleNameEditText))
                    .getText().toString();

            if(filterText.length() > 0)
                moduleFilter.filter(filterText);
            else
                moduleFilter.filter("");
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        moduleFilter.filter(s);
    }

    @Override
    public void afterTextChanged(Editable s) {}
}