package anna.mystudynotes.api;

import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


import anna.mystudynotes.main.MyStudyNotesApp;
import anna.mystudynotes.models.Module;
import anna.mystudynotes.models.User;


public class DBManager {

    private static final String TAG = "DB Manager";
    public DatabaseReference mFirebaseDatabase;
    public static String mFBUserId;
    public DBListener mDBListener;
    public MyStudyNotesApp app = MyStudyNotesApp.getInstance();


    public void open() {
        //Set up local caching
        FirebaseApp.initializeApp(MyStudyNotesApp.getInstance());

        //Bind to remote Firebase Database
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();
        Log.v(TAG, "Remote Firebase Database Connected :" + mFirebaseDatabase);
    }

    public void attachListener(DBListener listener) {
        mDBListener = listener;
    }

    //Check to see if the Firebase User exists in the Database
    //if not, create a new User
    public void checkUser(final String userid,final String username,final String email) {
        Log.v(TAG, "checkUser ID == " + userid);
        mFirebaseDatabase.child("users").child(userid).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.v(TAG, "checkUser ID Sucess  before if== " + dataSnapshot);
                        if(dataSnapshot.exists()){
                            Log.v(TAG, "User found : ");
                        }
                        else{
                            Log.v(TAG, "User not found, Creating User on Firebase");
                            User newUser = new User(app.mFirebaseUser.getUid(),
                                    app.mFirebaseUser.getDisplayName(),
                                    app.mFirebaseUser.getEmail(), null);
                                app.dbManager.mFirebaseDatabase.child("users")
                                        .child(app.mFirebaseUser.getUid())
                                        .setValue(newUser);
                        }
                        app.dbManager.mFBUserId = app.mFirebaseUser.getUid();
                        mDBListener.onSuccess(dataSnapshot);
                        Log.v(TAG, "checkUser ID Sucess == " + dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        mDBListener.onFailure();
                        Log.v(TAG, "checkUser ID Failure == " + databaseError);
                    }
                }
        );
    }


    public void addModule(final Module m)
    {
        mFirebaseDatabase.child("users").child(app.dbManager.mFBUserId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        User user = dataSnapshot.getValue(User.class);
                        if (user == null) {
                            // User is null, error out
                            Log.v(TAG, "User " + mFBUserId + " is unexpectedly null");
                        } else {
                            writeNewModule(m);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.v(TAG, "getUser:onCancelled", databaseError.toException());
                    }
                });
    }

    private void writeNewModule(Module m) {

        String key = mFirebaseDatabase.child("modules").push().getKey();

        Log.v(TAG, "writing new module" + m);
        //Adding to all modules
        app.dbManager.mFirebaseDatabase.child("modules")
                .child(key)
                .setValue(m);
        //All modules per user
        app.dbManager.mFirebaseDatabase.child("user-modules")
                .child(app.dbManager.mFBUserId)
                .child(key)
                .setValue(m);
    }

    public Query getAllModules()
    {
        Query query = mFirebaseDatabase.child("user-modules").child(app.dbManager.mFBUserId)
                .orderByChild("rating");

        return query;
    }

    public Query getCurrentModules()
    {
        Query query = mFirebaseDatabase.child("user-modules")
                .child(app.dbManager.mFBUserId)
                .orderByChild("current").equalTo(true);

        return query;
    }

    public void getAModule(final String moduleKey)
    {
        Log.v(TAG,"Calling a user from db manager get a module " + moduleKey);
        mFirebaseDatabase.child("user-modules").child(app.dbManager.mFBUserId ).child(moduleKey).addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.v(TAG,"The read Succeeded: " + dataSnapshot.toString());
                        mDBListener.onSuccess(dataSnapshot);
                    }
                    @Override
                    public void onCancelled(DatabaseError firebaseError) {
                        Log.v(TAG,"The read failed: " + firebaseError.getMessage());
                        mDBListener.onFailure();
                    }
                });
    }

    public void updateAModule(final String moduleKey,final  Module updatedModule)
    {
        mFirebaseDatabase.child("user-modules").child(mFBUserId).child(moduleKey).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.v(TAG,"The update Succeeded: " + dataSnapshot.toString());
                        dataSnapshot.getRef().setValue(updatedModule);
                    }
                    @Override
                    public void onCancelled(DatabaseError firebaseError) {
                        Log.v(TAG,"The update failed: " + firebaseError.getMessage());
                        mDBListener.onFailure();
                    }
                });
    }

    public void toggleFavourite(final String moduleKey,final boolean isFavourite)
    {
        mFirebaseDatabase.child("user-modules").child(mFBUserId).child(moduleKey).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        snapshot.getRef().child("favourite").setValue(isFavourite);
                    }
                    @Override
                    public void onCancelled(DatabaseError firebaseError) {
                        Log.v(TAG,"The toggle failed: " + firebaseError.getMessage());
                    }
                });
    }

    public void deleteAModule(final String moduleKey)
    {
        mFirebaseDatabase.child("user-modules").child(mFBUserId).child(moduleKey).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        snapshot.getRef().removeValue();
                    }
                    @Override
                    public void onCancelled(DatabaseError firebaseError) {
                        Log.v(TAG,"The delete failed: " + firebaseError.getMessage());
                    }
                });
    }

    public Query nameFilter(String s)
    {
        Query query = mFirebaseDatabase.child("user-modules").child(mFBUserId)
                .orderByChild("moduleName").startAt(s).endAt(s+"\uf8ff");

        return query;
    }

    public Query noteFilter(String s)
    {
        Query query = mFirebaseDatabase.child("user-modules").child(mFBUserId)
                .orderByChild("note").startAt(s).endAt(s+"\uf8ff");

        return query;
    }

    public void getAllModulesSnapshot()
    {
        ValueEventListener vel = mFirebaseDatabase.child("user-modules")
                                            .child(mFBUserId)
                                            .addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        mDBListener.onSuccess(dataSnapshot);
                    }
                    @Override
                    public void onCancelled(DatabaseError firebaseError) {
                        mDBListener.onFailure();
                    }
                });
    }

    //	public void addChangeListener(String userId)
//	{
//
//		Query allModulesQuery = fbDatabase.child("user-modules").child(userId)
//				.orderByChild("rating");
//
//
//
//		allModulesQuery.addChildEventListener(new ChildEventListener() {
//			// TODO: implement the ChildEventListener methods as documented
//
//			@Override
//			public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
//				Log.d(TAG, "onChildAdded:" + dataSnapshot.getKey());
//
//				// A new comment has been added, add it to the displayed list
//				 Module c = dataSnapshot.getValue(Module.class);
//				 FBDManager.moduleList.add(c);
//				//HashMap<String, Object> result = (HashMap<String, Object>)dataSnapshot.getValue();
//				Log.d(TAG, "dataSnapShop is :" + dataSnapshot);
//				Log.d(TAG, "moduleList is now :" + FBDManager.moduleList);
//
//				// ...
//			}
//
//			@Override
//			public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
//				Log.d(TAG, "onChildChanged:" + dataSnapshot.getKey());
//
//				// A comment has changed, use the key to determine if we are displaying this
//				// comment and if so displayed the changed comment.
//				Module c = dataSnapshot.getValue(Module.class);
//				String moduleKey = dataSnapshot.getKey();
//
//				// ...
//			}
//
//			@Override
//			public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
//				Log.d(TAG, "onChildMoved:" + dataSnapshot.getKey());
//
//				// A comment has changed position, use the key to determine if we are
//				// displaying this comment and if so move it.
//				Module c = dataSnapshot.getValue(Module.class);
//				String moduleKey = dataSnapshot.getKey();
//
//				// ...
//			}
//
//			@Override
//			public void onChildRemoved(DataSnapshot dataSnapshot) {
//				Log.d(TAG, "onChildRemoved:" + dataSnapshot.getKey());
//
//				// A comment has changed, use the key to determine if we are displaying this
//				// comment and if so remove it.
//				String commentKey = dataSnapshot.getKey();
//
//				// ...
//			}
//
//			@Override
//			public void onCancelled(DatabaseError databaseError) {
//				Log.w(TAG, "loadModules:onCancelled", databaseError.toException());
//				//Toast.makeText(mContext, "Failed to load modules.",
//				//		Toast.LENGTH_SHORT).show();
//			}
//		});
//	}
}
