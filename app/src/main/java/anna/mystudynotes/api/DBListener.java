package anna.mystudynotes.api;

import com.google.firebase.database.DataSnapshot;

public interface DBListener {
    void onSuccess(DataSnapshot dataSnapshot);
    void onFailure();
}
